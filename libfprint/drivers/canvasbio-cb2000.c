/*
 * Driver for CanvasBio CB2000
 * Copyright (C) 2023 Mahid Sheikh <mahidsheikh@proton.me>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 */

#include <glib.h>
#include "fp-device.h"
#include "fpi-device.h"
#include "fpi-ssm.h"
#include "fpi-usb-transfer.h"
#include "fpi-log.h"
#include "drivers_api.h"
#include "gusb/gusb-device.h"

#define FP_COMPONENT "canvasbio-cb2000"
#define FRAME_HEIGHT 64
#define FRAME_WIDTH 80

#define CB2000_OUT 0x01
#define CB2000_IN 0x82

#define CB2000_DEFAULT_TIMEOUT 500
#define CANVASBIO_VENDOR_ID 0x2df0

static const FpIdEntry id_table[] = {
  { .vid = CANVASBIO_VENDOR_ID,  .pid = 0x0003,  },
};

struct _FpiDeviceCb2000 
{
  FpImageDevice parent;
  FpiSsm *ssm;
};

G_DECLARE_FINAL_TYPE (FpiDeviceCb2000, fpi_device_cb2000, FPI, DEVICE_CB2000, FpImageDevice);
G_DEFINE_TYPE (FpiDeviceCb2000, fpi_device_cb2000, FP_TYPE_IMAGE_DEVICE);

/*
 * CavasBio's protocol is extremely horrifying, with a ton of 
 * weird stuff and extremely long commands that makes this
 * driver a contendor for the title of "most horrific protocol 
 * reversed engineered for a libfprint driver". This is a protocol
 * no human would be crazy enough to write, and I am 95% convinced 
 * that this protocol was made by some psycopathic aliens.
 *
 * I beg the almighty God that no person in the future 
 * will have to deal with CanvasBio sensors again.
 *
 * - Mahid Sheikh, 14th December, 2023
 */

// States for waiting for finger
typedef enum {
  AWAIT_1 = 0,  AWAIT_2,  AWAIT_3, 
  AWAIT_4,  AWAIT_5,  AWAIT_6, 
  AWAIT_7,  AWAIT_8,  AWAIT_9, 
  AWAIT_10, AWAIT_11, AWAIT_12, 
  AWAIT_13, AWAIT_14, AWAIT_15, 
  AWAIT_16, AWAIT_17, AWAIT_18, 
  AWAIT_19, AWAIT_20, AWAIT_21, 
  AWAIT_22, AWAIT_23, AWAIT_24, 
  AWAIT_25, AWAIT_26, AWAIT_27, 
  AWAIT_28, AWAIT_29, AWAIT_30, 
  AWAIT_31, AWAIT_32
} CbAwaitStates;

// You'll see these nested arrays a lot, because
// CanvasBio uses tons of bytes in their USB calls
//
// We set this to be a dynamic array of arrays of 
// size 4, because we have at max 4 bytes . While some
// memory will be wasted as a result, it's a small 
// ammount for modern operating systems
static guint8 cb2000_cmd_await_finger[][4] = {
  {0x4f, 0x80},             {0xa9, 0x4f, 0x80},       {0xa8, 0xb9, 0x00}, 
  {0xa9, 0x50, 0x12, 0x00}, {0xa9, 0x5f, 0x00, 0x00}, {0xa9, 0x4e, 0x02, 0x00}, 
  {0xa9, 0x60, 0x21, 0x00}, {0xa9, 0x61, 0x70, 0x00}, {0xa9, 0x62, 0x00, 0x21}, 
  {0xa9, 0x63, 0x00, 0x21}, {0xa9, 0x64, 0x04, 0x08}, {0xa9, 0x65, 0x85, 0x08}, 
  {0xa9, 0x66, 0x0d, 0x00}, {0xa9, 0x66, 0x0d, 0x00}, {0xa9, 0x68, 0x00, 0x0c}, 
  {0xa9, 0x68, 0x00, 0x0c}, {0xa9, 0x6b, 0x11, 0x70}, {0xa9, 0x6c, 0x00, 0x0e}, 
  {0xa9, 0x09, 0x00, 0x00}, {0xa9, 0x5d, 0x3d, 0x00}, {0xa9, 0x51, 0xa8, 0x01}, 
  {0xa9, 0x03, 0x00},       {0xa9, 0x38, 0x01, 0x00}, {0xa9, 0x10, 0x60, 0x00}, 
  {0xa9, 0x3b, 0x14, 0x00}, {0xa9, 0x3d, 0xff, 0x0f}, {0xa9, 0x26, 0x30, 0x00}, 
  {0xa9, 0x2f, 0xf6, 0xff}, {0xa9, 0x09, 0x00, 0x00}, {0xa9, 0x0c, 0x00}, 
  {0xa8, 0x20, 0x00, 0x00}, {0xa9, 0x04, 0x00}
};

// Command to capture an image after finger is placed
//
// Again, a small amount of memory allocated here is 
// unused, but that's not a big deal
static guint8 cb2000_cmd_precapture[][4] = {
  {0xa8, 0x08, 0x00},       {0xa9, 0x09, 0x00, 0x00}, {0xa8, 0x3e, 0x00, 0x00}, 
  {0xa9, 0x03, 0x00, 0x00}, {0xa8, 0x20, 0x00, 0x00}, {0xa9, 0x0d, 0x00}, 
  {0xa9, 0x10, 0x00, 0x01}, {0xa9, 0x26, 0x00, 0x00}, {0xa9, 0x09, 0x00, 0x00}, 
  {0xa9, 0x0c, 0x00},       {0xa8, 0x20, 0x00, 0x00}, {0xa9, 0x51, 0x88, 0x01}, 
  {0xa9, 0x04, 0x00, 0x00}, {0xa9, 0x09, 0x00, 0x00}
};

// This is 259 bytes long, and technically should be part
// of the previous command, but that would be a lot of wasted
// space
//
// This is the reason as to why at the start of the file I beg to God to 
// make this the last time someone dealt with a CanvasBio sensor
#define CB_FINAL_PRECAPTURE_COMMAND_LEN 259
static guint8 cb2000_cmd_precapture_final[CB_FINAL_PRECAPTURE_COMMAND_LEN] = {
  0xa8, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00
};

// Just like with precapture, the purpose
// of these is unknown
static guint8 cb2000_cmd_postcapture[][4] = {
  {0xa9, 0x09, 0x00, 0x00}, {0xa8, 0x3e, 0x00, 0x00}, 
  {0xa8, 0x3e, 0x00, 0x00}, {0xa9, 0x0d, 0x00}
};

// Reusable function for control in
static void cb2000_control_transfer_in(FpDevice* device, FpiSsm* ssm, 
                                        guint8 request, guint16 value, guint16 idx, gsize length) {
  g_autoptr(FpiUsbTransfer) ctrl_in_transfer = fpi_usb_transfer_new(device);
  fpi_usb_transfer_fill_control(ctrl_in_transfer, 
                              G_USB_DEVICE_DIRECTION_DEVICE_TO_HOST, 
                              G_USB_DEVICE_REQUEST_TYPE_VENDOR, 
                              G_USB_DEVICE_RECIPIENT_DEVICE, 
                              request, value, idx, length);
  ctrl_in_transfer->ssm = ssm;
  ctrl_in_transfer->short_is_error = TRUE;
  fpi_usb_transfer_submit(ctrl_in_transfer, 
                          CB2000_DEFAULT_TIMEOUT, 
                          NULL, 
                          fpi_ssm_usb_transfer_cb, 
                          NULL);
}

// Reusable function for control out
static void cb2000_control_transfer_out(FpDevice* device, FpiSsm* ssm, 
                                        guint8 request, guint16 value, guint16 idx, gsize length) {
  g_autoptr(FpiUsbTransfer) ctrl_out_transfer = fpi_usb_transfer_new(device);
  fpi_usb_transfer_fill_control(ctrl_out_transfer, 
                                G_USB_DEVICE_DIRECTION_HOST_TO_DEVICE, 
                                G_USB_DEVICE_REQUEST_TYPE_VENDOR, 
                                G_USB_DEVICE_RECIPIENT_DEVICE, 
                                request, value, idx, length);
  ctrl_out_transfer->ssm = ssm;
  ctrl_out_transfer->short_is_error = TRUE;
  fpi_usb_transfer_submit(ctrl_out_transfer, 
                          CB2000_DEFAULT_TIMEOUT, 
                          NULL, 
                          fpi_ssm_usb_transfer_cb, 
                          NULL);
}

static void cb2000_await_finger_run_state(FpiSsm *ssm, FpDevice *device) {
  fp_dbg("Executing Init Sequence");

  // For this command, everything has a 
  // bulk transfer that goes out, so we 
  // can make this here
  g_autoptr(FpiUsbTransfer) bulk_transfer = fpi_usb_transfer_new(device);
  switch (fpi_ssm_get_cur_state(ssm)) {
    case AWAIT_1:
      fpi_usb_transfer_fill_bulk_full(bulk_transfer, 
                                      CB2000_OUT, 
                                      cb2000_cmd_await_finger[0], 
                                      2, 
                                      NULL);
      bulk_transfer->ssm = ssm;
      bulk_transfer->short_is_error = TRUE;
      fpi_usb_transfer_submit(bulk_transfer, 
                              CB2000_DEFAULT_TIMEOUT, 
                              NULL, 
                              fpi_ssm_usb_transfer_cb, 
                              NULL);
      cb2000_control_transfer_in(device, ssm, 
                                 204, 0x0000, 0, 4);
      cb2000_control_transfer_out(device, ssm, 
                                  202, 0x0002, 3, 0);
      break;
    case AWAIT_2:
      fpi_usb_transfer_fill_bulk_full(bulk_transfer, 
                                      CB2000_OUT, 
                                      cb2000_cmd_await_finger[1], 
                                      3, 
                                      NULL);
      
      bulk_transfer->ssm = ssm;
      bulk_transfer->short_is_error = TRUE;
      fpi_usb_transfer_submit(bulk_transfer, 
                              CB2000_DEFAULT_TIMEOUT, 
                              NULL, 
                              fpi_ssm_usb_transfer_cb, 
                              NULL);
      cb2000_control_transfer_in(device, ssm, 
                                 204, 0x0000, 0, 4);
      cb2000_control_transfer_out(device, ssm, 
                                  202, 0x0003, 3, 0);
      break;
    case AWAIT_3:
      fpi_usb_transfer_fill_bulk_full(bulk_transfer, 
                                      CB2000_OUT, 
                                      cb2000_cmd_await_finger[2], 
                                      3, 
                                      NULL);
      bulk_transfer->ssm = ssm;
      bulk_transfer->short_is_error = TRUE;
      fpi_usb_transfer_submit(bulk_transfer, 
                              CB2000_DEFAULT_TIMEOUT, 
                              NULL, 
                              fpi_ssm_usb_transfer_cb, 
                              NULL);

      // This part of the await command 
      // reads in a 67 byte long sequence
      {
        g_autoptr(FpiUsbTransfer) read_bulk_transfer = fpi_usb_transfer_new(device);
        read_bulk_transfer->ssm = ssm;
        read_bulk_transfer->short_is_error = TRUE;
        fpi_usb_transfer_fill_bulk(bulk_transfer, CB2000_IN, 67);
        fpi_usb_transfer_submit(read_bulk_transfer, 
                                CB2000_DEFAULT_TIMEOUT, 
                                NULL, 
                                fpi_ssm_usb_transfer_cb, 
                                NULL);
      }
      cb2000_control_transfer_out(device, ssm, 
                                  202, 0x0002, 4, 0);
      break;

    case AWAIT_4:
    case AWAIT_5:
    case AWAIT_6:
    case AWAIT_7:
    case AWAIT_8:
    case AWAIT_9:
    case AWAIT_10:
    case AWAIT_11:
    case AWAIT_12:
    case AWAIT_13:
    case AWAIT_14:
    case AWAIT_15:
    case AWAIT_16:
    case AWAIT_17:
    case AWAIT_18:
      fpi_usb_transfer_fill_bulk_full(bulk_transfer, 
                                      CB2000_OUT, 
                                      cb2000_cmd_await_finger[fpi_ssm_get_cur_state(ssm)], 
                                      4, 
                                      NULL);
      bulk_transfer->ssm = ssm;
      bulk_transfer->short_is_error = TRUE;
      fpi_usb_transfer_submit(bulk_transfer, 
                              CB2000_DEFAULT_TIMEOUT, 
                              NULL, 
                              fpi_ssm_usb_transfer_cb, 
                              NULL);
      cb2000_control_transfer_in(device, ssm, 
                                 204, 0x0000, 0, 4);
      cb2000_control_transfer_out(device, ssm, 
                                  202, 0x0002, 4, 0);
      break;
    case AWAIT_19:
      fpi_usb_transfer_fill_bulk_full(bulk_transfer, 
                                      CB2000_OUT, 
                                      cb2000_cmd_await_finger[4], 
                                      4, 
                                      NULL);
      bulk_transfer->ssm = ssm;
      bulk_transfer->short_is_error = TRUE;
      fpi_usb_transfer_submit(bulk_transfer, 
                              CB2000_DEFAULT_TIMEOUT, 
                              NULL, 
                              fpi_ssm_usb_transfer_cb, 
                              NULL);
      cb2000_control_transfer_in(device, ssm, 
                                 204, 0x0000, 0, 4);
      cb2000_control_transfer_out(device, ssm, 
                                  219, 0x0001, 1, 0);
      cb2000_control_transfer_in(device, ssm, 
                                 218, 0x00fe, 0, 2);
      cb2000_control_transfer_in(device, ssm, 
                                 218, 0x00ff, 0, 2);
      cb2000_control_transfer_out(device, ssm, 
                                  202, 0x0002, 4, 0);
      break;
  }
}
